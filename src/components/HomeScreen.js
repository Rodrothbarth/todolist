import Icon from 'react-native-vector-icons/dist/FontAwesome';
import React, { useEffect, useState } from 'react';
import api from '../services/axios';
import {
    StyleSheet,
    FlatList,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
} from 'react-native';

const toDoList = ({navigation}) => {

  const [tarefas, setTarefa] = React.useState(false);
  


  useEffect(async () => {
    try{
      const response = await api.get('/todos');
      console.log(JSON.stringify(response));
      setTarefa(response.data);
    } catch (error) {
      console.log("DEU RUIM" + error);
    }
  }, [])

  const TextTarefa = ({item}) => {
    return(
      <View>
        <TouchableOpacity onPress={()=> navigation.navigate('Update')}>
          
          <View style={styles.blocoTarefa}>
                 <View style={styles.blocoEsquerda}>
                  <Text style={styles.titulo}>
                  {item.titulo}
                   </Text>
                  <Text style={styles.descricao}>
                  {item.descricao}
                   </Text>
                </View>
                <View> 
                <Icon name="trash" size={30} color="#900" />
                </View>
               <View style={styles.blocoDireita}>
                   <View style={styles.blocoData}>
                     <Text style={styles.data}>
                     {item.date}
                      </Text>
                 </View>
                 
               </View>
              </View>
        </TouchableOpacity>
      </View>
    )
  }

    return (
    
        <View style={styles.container}>
            <View style={styles.MinhasTarefas}>
                <Text style={styles.navegarPlus} onPress={()=> navigation.navigate('Add')}>
                    +
                </Text>

                <Text style={styles.tituloMT} onPress={()=> navigation.navigate('Add')}>
                    Adicionar Tarefas
                </Text>
            </View>

            <Text style={styles.nomeLista}>Lista de Tarefas:</Text>
        
            <FlatList
                style={{color: '#000'}}
                    data={tarefas}
                    renderItem={TextTarefa}
                    keyExtractor={ tarefa => tarefa.titulo}
            />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    MinhasTarefas: {
        backgroundColor: '#00A6A6',
        padding: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        
        elevation: 5,
    },

    tituloMT: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold',

    },

    navegarPlus: {
        fontSize: 30,
        color: '#fff',
        fontWeight: 'bold',
        marginRight: 10,
    },

    nomeLista:{
        fontSize: 20,
        marginTop: 15,
        marginBottom: 20,
        paddingLeft: 10,
        fontStyle: 'italic',
    },

  blocoTarefa: {
    flex: 1,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#cecece',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  blocoEsquerda: {
    width: 340,
    padding: 10,
  },

  titulo: {
    fontSize: 20,
    fontStyle: 'italic',
    fontWeight: 'bold',
    marginBottom: 5,
  },

  blocoDireita: {
    backgroundColor: '#00A6A6',
    width: 60,
  },  
  blocoData: {
    padding: 5,

  },
  data: {
    fontSize: 16,
    color: '#fff',
    fontWeight: 'bold',
  },
  // container: {
  //   flex: 1,
  //   borderColor: 'blue',
  //   borderWidth: 1,
  //   alignContent: 'center',
  //   justifyContent: 'center',
  //   alignItems: 'center',

  // },
  input:{
    width: 200,
    height: 40,
    marginVertical: 10,
    borderColor:'black',
    borderWidth: 1,
    borderRadius: 20,
    paddingLeft: 20
  },
  button: {
    borderColor: '#ffd700',
    borderWidth: 1,
    borderRadius: 1,
    backgroundColor: '#ffd700',
    width: 100,
    height: 25,
    alignItems: 'center',
    marginTop: 15,
  },
  buttonText:{
    color:"#000"
  },
  tarefaTitulo: {
    borderColor: '#9e9e9e',
    borderTopWidth: 1,
    paddingVertical: 20,
    width: 200,
    textAlign: 'center'
  },
  nomeLista:{
    fontSize: 24,
    marginTop: 30,
    marginBottom: 20
  },
  navegarAdd: {
    borderWidth: 1,
    backgroundColor: '#000',
  },
})

export default toDoList;