// import React from 'react';
// import AddTarefa from './components/AddTarefa';
// const App = () => <AddTarefa />;
// export default App;

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import { StyleSheet } from 'react-native';

import toDoList from './components/HomeScreen';
import AddTarefa from './components/AddTarefa';
import UpdateTarefa from './components/UpdateTarefa';

const Page = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Page.Navigator>
        <Page.Screen 
          name='Tarefas' 
          component={toDoList} 
          options={{
            headerTintColor: "white",
            headerStyle: {
              backgroundColor: "#EFCA08",
            }
          }}
        />
        <Page.Screen 
          name='Add' 
          component={AddTarefa}
          options={{
            headerTintColor: "white",
            headerStyle: {
              backgroundColor: "#EFCA08"
            }
          }}
        />
        <Page.Screen 
          name="Update" 
          component={UpdateTarefa}
          options={{
            headerTintColor: "white",
            headerStyle: {
              backgroundColor: "#EFCA08"
            }
          }}
        />
      </Page.Navigator>
    </NavigationContainer>
  );
}


//  const styles = StyleSheet.create({
//   header: {
//     backgroundColor: '#EFCA08', 
//     padding: 100, 
//   }
//  })

 export default App;
